using Homework30;
using NUnit.Framework;
using System;

namespace Test
{
    public class ICloneableTest
    {
        private static readonly Cat cat = new Cat("Murzik", 1, "british");

        [Test]
        public void TestReferenceNotEqual()
        {
            Cat catClone = (Cat)((ICloneable)cat).Clone();
            Assert.IsFalse(ReferenceEquals(cat, catClone));
        }

        [Test]
        public void TestReferenceNotEqualBugged()
        {
            CatBugged catBugged = new CatBugged("Murzik", 1, "british");
            CatBugged catBuggedClone = (CatBugged)((ICloneable)catBugged).Clone(); // runtime error

            Assert.IsFalse(ReferenceEquals(cat, catBuggedClone));
        }

        [Test]
        public void TestEquality()
        {
            Cat catClone = (Cat)((ICloneable)cat).Clone();

            Assert.AreEqual(cat.Name, catClone.Name);
            Assert.AreEqual(cat.Animate, catClone.Animate);
            Assert.AreEqual(cat.Weight, catClone.Weight);
            Assert.AreEqual(cat.Breed, catClone.Breed);
        }
    }
}