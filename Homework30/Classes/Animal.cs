﻿namespace Homework30
{
    public class Animal : Object, IMyCloneable<Animal>
    {
        public Animal(string name, int weight) : base(true, name, weight)
        {
        }
        
        public override Animal Clone()
        {
            return new Animal(Name, Weight);
        }
    }
}
