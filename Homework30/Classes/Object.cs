﻿using System;

namespace Homework30
{
    public class Object : IMyCloneable<Object>, ICloneable
    {
        public bool Animate;
        public string Name;
        public int Weight;

        public Object(bool animate, string name, int weight)
        {
            Animate = animate;
            Name = name;
            Weight = weight;
        }

        public virtual Object Clone()
        {
            return new Object(Animate, Name, Weight);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
