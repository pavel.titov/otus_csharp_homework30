﻿namespace Homework30
{
    public class Cat : Animal, IMyCloneable<Cat>
    {
        public string Breed;

        public Cat(string name, int weight, string breed) : base(name, weight)
        {
            Breed = breed;
        }

        public override Cat Clone()
        {
            return new Cat(Name, Weight, Breed);
        }
    }
}
