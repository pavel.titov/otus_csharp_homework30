﻿namespace Homework30
{
    public class CatBugged : Animal
    {
        public string Breed;

        public CatBugged(string name, int weight, string breed) : base(name, weight)
        {
            Breed = breed;
        }
    }
}
