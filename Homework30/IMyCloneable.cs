﻿namespace Homework30
{
    interface IMyCloneable<T>
    {
        T Clone();
    }
}
